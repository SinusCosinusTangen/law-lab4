from django.db import models
from django.forms import CharField

# Create your models here.
class Mahasiswa(models.Model):
    nama = models.CharField(max_length=50, default="null")
    alamat = models.CharField(max_length=100, default="null")
    npm = models.CharField(max_length=10, primary_key=True)
    photo = models.FileField(null=True, upload_to='mahasiswa/media/images')